﻿namespace Client
{
    partial class AccountManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.balanceAccount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.DepositValue = new System.Windows.Forms.TextBox();
            this.DepositButton = new System.Windows.Forms.Button();
            this.WithdrawValue = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.WithdrawButton = new System.Windows.Forms.Button();
            this.LogoutButton = new System.Windows.Forms.Button();
            this.HistoryOperation = new System.Windows.Forms.DataGridView();
            this.RefeshButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryOperation)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Witamy w aplikacji Banku";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Stan konta:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // balanceAccount
            // 
            this.balanceAccount.Enabled = false;
            this.balanceAccount.Location = new System.Drawing.Point(60, 65);
            this.balanceAccount.Name = "balanceAccount";
            this.balanceAccount.Size = new System.Drawing.Size(100, 20);
            this.balanceAccount.TabIndex = 2;
            this.balanceAccount.TextChanged += new System.EventHandler(this.balanceAccount_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(57, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Wpłać pieniądze na konto";
            // 
            // DepositValue
            // 
            this.DepositValue.Location = new System.Drawing.Point(60, 123);
            this.DepositValue.Name = "DepositValue";
            this.DepositValue.Size = new System.Drawing.Size(100, 20);
            this.DepositValue.TabIndex = 4;
            // 
            // DepositButton
            // 
            this.DepositButton.Location = new System.Drawing.Point(60, 150);
            this.DepositButton.Name = "DepositButton";
            this.DepositButton.Size = new System.Drawing.Size(75, 23);
            this.DepositButton.TabIndex = 5;
            this.DepositButton.Text = "Wpłać";
            this.DepositButton.UseVisualStyleBackColor = true;
            this.DepositButton.Click += new System.EventHandler(this.DepositButton_Click);
            // 
            // WithdrawValue
            // 
            this.WithdrawValue.Location = new System.Drawing.Point(57, 206);
            this.WithdrawValue.Name = "WithdrawValue";
            this.WithdrawValue.Size = new System.Drawing.Size(100, 20);
            this.WithdrawValue.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(57, 190);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Wypłać pieniądze z konta";
            // 
            // WithdrawButton
            // 
            this.WithdrawButton.Location = new System.Drawing.Point(60, 233);
            this.WithdrawButton.Name = "WithdrawButton";
            this.WithdrawButton.Size = new System.Drawing.Size(75, 23);
            this.WithdrawButton.TabIndex = 8;
            this.WithdrawButton.Text = "Wypłać";
            this.WithdrawButton.UseVisualStyleBackColor = true;
            this.WithdrawButton.Click += new System.EventHandler(this.WithdrawButton_Click);
            // 
            // LogoutButton
            // 
            this.LogoutButton.Location = new System.Drawing.Point(639, 372);
            this.LogoutButton.Name = "LogoutButton";
            this.LogoutButton.Size = new System.Drawing.Size(75, 23);
            this.LogoutButton.TabIndex = 9;
            this.LogoutButton.Text = "Wyloguj";
            this.LogoutButton.UseVisualStyleBackColor = true;
            this.LogoutButton.Click += new System.EventHandler(this.LogoutButton_Click);
            // 
            // HistoryOperation
            // 
            this.HistoryOperation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HistoryOperation.Location = new System.Drawing.Point(279, 49);
            this.HistoryOperation.Name = "HistoryOperation";
            this.HistoryOperation.Size = new System.Drawing.Size(375, 207);
            this.HistoryOperation.TabIndex = 10;
            // 
            // RefeshButton
            // 
            this.RefeshButton.Location = new System.Drawing.Point(279, 18);
            this.RefeshButton.Name = "RefeshButton";
            this.RefeshButton.Size = new System.Drawing.Size(75, 23);
            this.RefeshButton.TabIndex = 11;
            this.RefeshButton.Text = "Refresh";
            this.RefeshButton.UseVisualStyleBackColor = true;
            this.RefeshButton.Click += new System.EventHandler(this.RefeshButton_Click);
            // 
            // AccountManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.RefeshButton);
            this.Controls.Add(this.HistoryOperation);
            this.Controls.Add(this.LogoutButton);
            this.Controls.Add(this.WithdrawButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.WithdrawValue);
            this.Controls.Add(this.DepositButton);
            this.Controls.Add(this.DepositValue);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.balanceAccount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AccountManager";
            this.Text = "Bank Client";
            ((System.ComponentModel.ISupportInitialize)(this.HistoryOperation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox balanceAccount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox DepositValue;
        private System.Windows.Forms.Button DepositButton;
        private System.Windows.Forms.TextBox WithdrawValue;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button WithdrawButton;
        private System.Windows.Forms.Button LogoutButton;
        private System.Windows.Forms.DataGridView HistoryOperation;
        private System.Windows.Forms.Button RefeshButton;
    }
}