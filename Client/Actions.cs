﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Server;
using Server.ModelsDto;

namespace Client
{
    class Actions : IActions
    {
        private readonly IBank _server;
        private string _token;
        public Actions()
        {
            _server = (IBank) Activator.GetObject(typeof(IBank), "tcp://localhost:8888/remote");
        }
        public bool Autorize(int number, int pin)
        {
            var response = _server.Authorize(number, pin);

            if (response == null)
            {
                return false;
            }

            _token = response;
            return true;
        }

        public double GetBalance()
        {
            if (_token == null)
            {
                throw new NotImplementedException();
            }
            var balance =_server.getBalance(_token);

            return balance;
        }

        public bool deposit(double value)
        {
            return _server.deposit(_token, value);
        }

        public bool withdraw(double value)
        {
            var withdraw = _server.withdraw(_token, value);
            if (withdraw)
            {
                return true;
            }
            return false;
        }

        public void logout()
        {
            _server.logout(_token);
            _token = null;
        }

        public OperationDto[] GetHistoryOperation()
        {
            string [] history = _server.GetOperationHistory(_token);
            OperationDto[] messages = new OperationDto[]{};

            if (history != null)
            {
                messages = history.Select(x => new OperationDto(x)).ToArray();
            }
            
            return messages;
        }
    }
}
