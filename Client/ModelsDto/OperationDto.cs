﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.ModelsDto
{
    public class OperationDto
    {
        public string Description { get; set; }
        public string DataOperation { get; set; }

        public OperationDto(string messages)
        {
            string[] text = messages.Split(';');
            DataOperation = text[0] ;
            Description = text[1];
        }
    }
}
