﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Server;
using Server.ModelsDto;

namespace Client
{
    public interface IActions
    {
        bool Autorize(int number, int pin);
        double GetBalance();
        bool deposit(double value);
        bool withdraw(double value);
        void logout();
        OperationDto[] GetHistoryOperation();
    }
}
