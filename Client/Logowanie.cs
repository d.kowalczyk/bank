﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class Logowanie : Form
    {
        private IActions _actions;
        public Logowanie()
        {
            InitializeComponent();
            _actions = new Actions();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (Numer_konta.Text == "")
            {
                var result = MessageBox.Show("Podaj numer konta",
                    "Wystąpił błąd!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Question);
            }
            else if (PIN.Text == "")
            {
                var result = MessageBox.Show("Podaj pin",
                    "Wystąpił błąd!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Question);
            }
            else
            {
                int numer;
                int.TryParse(Numer_konta.Text, out numer);
                int pin;
                int.TryParse(PIN.Text, out pin);
                var auth = _actions.Autorize(numer, pin);


                if (auth)
                {
                    this.Hide();
                    Console.WriteLine("Zalogowano poprawnie.");
                    AccountManager manager = new AccountManager(this._actions);
                    manager.ShowDialog();
                    this.Show();
                }
                else
                {
                    var result = MessageBox.Show("Niepoprawne dane logowania",
                        "Wystąpił błąd!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Question);
                }
            }
        }
    }
}
