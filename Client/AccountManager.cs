﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Server;
using Server.Models;
using Server.ModelsDto;

namespace Client
{
    public partial class AccountManager : Form
    {
        private readonly IActions _actions;
        public AccountManager()
        {
            InitializeComponent();
               
        }

        public AccountManager(IActions server)
        {
            _actions = server;
            InitializeComponent();
            GetInformation();
        }

        private void GetInformation()
        {
            var balance = _actions.GetBalance();
            balanceAccount.Text = balance.ToString();

            var operations = _actions.GetHistoryOperation();


            HistoryOperation.Rows.Clear();
            HistoryOperation.Columns.Clear();
            HistoryOperation.ColumnCount = 2;
            HistoryOperation.Columns[0].Name = "Data";
            HistoryOperation.Columns[1].Name = "Operacja";

            if (operations != null)
            {
                foreach(OperationDto item in operations)
                {
                    HistoryOperation.Rows.Add(item.DataOperation, item.Description);
                }
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void balanceAccount_TextChanged(object sender, EventArgs e)
        {
 
        }

        private void DepositButton_Click(object sender, EventArgs e)
        {
            double value = Double.Parse(DepositValue.Text);
            if (!_actions.deposit(value))
            {
                var result = MessageBox.Show("Nie udało się dokonać wpłaty.",
                    "Błąd tranzakcji!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Question);
            }
            GetInformation();
        }

        private void WithdrawButton_Click(object sender, EventArgs e)
        {
            double value = Double.Parse(WithdrawValue.Text);
            var withdraw = _actions.withdraw(value);
            if (withdraw)
            {
                GetInformation();
                Console.WriteLine($"Wypłacono {value}");
            }
            else
            {
                var result = MessageBox.Show("Brak wystarczających środków na koncie",
                    "Błąd tranzakcji!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Question);
                Console.WriteLine($"Nie udało się wypłacić {value}. Zbyt mało środków na koncie");
            }
        }

        private void LogoutButton_Click(object sender, EventArgs e)
        {
            _actions.logout();
            this.Close();
        }

        private void RefeshButton_Click(object sender, EventArgs e)
        {
            GetInformation();
        }
    }
}
