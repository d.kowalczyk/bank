﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Security.Cryptography.X509Certificates;
using Server.Models;

namespace Server
{
    public interface IBankAction{

    }



    public class DataManager
    {
        private readonly List<Account> _account;
        private List<Token> _token = new List<Token>();
        private List<Operations> _operations = new List<Operations>();
        public DataManager()
        {
            _account = new List<Account>()
            {
                new Account()
                {
                    Number = 123,
                    Pin = 1234,
                    Balance = 12.00
                },
                new Account()
                {
                    Number = 444,
                    Pin = 1234,
                    Balance = 48.00
                },
            };
            Console.WriteLine("Database connected");
        }

        public Account getUser(int number, int pin)
        {
            Account account = this._account.Find(x => x.Number == number && x.Pin == pin);
            if(account == null)
            {
                return null;
            }

            return account;
        }
        public string generateToken(Account account)
        {
//            string token = "test";
            string token = Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 10);
            _token.Add(new Token()
            {
                token = token,
                account = account
            });
            return token;
        }

        public Account checkToken(string token)
        {
            Account account = _token.First(x => x.token == token).account;
            return account;
        }

        public void deleteToken(string token)
        {
            _token.RemoveAll(x => x.token == token);
        }

        public Operations[] GetUserOperationHistory(Account account)
        {
            if (account == null)
            {
                return new Operations[]{};
            }
            Operations [] history = _operations.Where(x => x.Account.Number == account.Number).ToArray();

            return history;
        }

        public void AddOperation(Account account, string text)
        {
            Operations operations = new Operations()
            {
                Account = account,
                Description = text,
                DataOperation = DateTime.Now
            };
            _operations.Add(operations);
        }
    }
}
