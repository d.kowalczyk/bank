﻿using System.Collections.Generic;

namespace Server
{
    public interface IBank
    {
        string Authorize(int account, int pin);
        double getBalance(string token);
        bool deposit(string token, double value);
        bool withdraw(string token, double value);
        void logout(string token);
        string[] GetOperationHistory(string token);
    }
}
