﻿using System;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;

namespace Server
{
    public class Server
    {
        public static void Main(string[] args)
        {
            TcpChannel channel = new TcpChannel(8888);


            ChannelServices.RegisterChannel(channel);

            RemotingConfiguration.RegisterWellKnownServiceType(typeof(global::Server.Bank), "remote", WellKnownObjectMode.Singleton);

            Console.WriteLine("The Server is ready .... Press the enter key to exit...");

            Console.ReadLine();

        }

    }
}
