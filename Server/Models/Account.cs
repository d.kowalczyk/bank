﻿namespace Server.Models
{
    public class Account
    {
        public double Balance { get; set; }
        public int Number { get; set; }
        public int Pin { get; set; }
        
    }
}
