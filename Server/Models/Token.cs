﻿namespace Server.Models
{
    public class Token
    {
        public string token { get; set; }
        public Account account { get; set; }
    }
}
