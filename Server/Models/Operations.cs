﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Server.Models;

namespace Server
{
    public class Operations
    {
        public string Description { get; set; }
        public DateTime DataOperation { get; set; }
        public Account Account { get; set; }
    }
}
