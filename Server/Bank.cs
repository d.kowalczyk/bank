﻿using System;
using System.Collections.Generic;
using System.Linq;
using Server.Models;


namespace Server
{
    public class Bank : MarshalByRefObject, IBank
    {
        private readonly DataManager _db;

        public Bank()
        {
            _db = new DataManager();
            Console.WriteLine("Server created");
        }

        public string Authorize(int account, int pin)
        {
            Account user = _db.getUser(account, pin);
            if (user == null)
            {
                Console.WriteLine("Błąd autoryzacji");
                return null;
            }

            string token = _db.generateToken(user);
            Console.WriteLine(token);

            return token;
        }

        public double getBalance(string token)
        {
            Account user = _db.checkToken(token);
            if (user == null)
            {
                return 0;
            }
            return user.Balance;
        }

        public bool deposit(string token, double value)
        {
            Account user = _db.checkToken(token);
            if (user != null)
            {
                user.Balance += value;
                _db.AddOperation(user, $"Numer {user.Number} wpłacił {value}");
                return true;
            }
            Console.WriteLine($"Nie udało się wwpłacić pieniędzy. Nieznane konto.");
            return false;
        }

        public bool withdraw(string token, double value)
        {
            Account user = _db.checkToken(token);
            if (user == null)
            {
                Console.WriteLine($"Nie udało się wwpłacić pieniędzy. Nieznane konto.");
                return false;
            }

            if (user.Balance < value)
            {
                Console.WriteLine($"Nie udało się wwpłacić pieniędzy. Brak środków na koncie.");
                return false;
            }

            user.Balance -= value;
            _db.AddOperation(user,$"Numer {user.Number} wypłacił {value}");
            
            return true;
        }

        public void logout(string token)
        {

            _db.deleteToken(token);
            Console.WriteLine($"Użytkownik wylogowany. Klucz {token} nieaktywny.");
        }

        public string[] GetOperationHistory(string token)
        {
            Account user = _db.checkToken(token);
            Operations[] operations = _db.GetUserOperationHistory(user);

            string [] messages = new string[]{};
            if (operations == null)
            {
                return null;
            }
            messages = operations.Select(x => $"{x.DataOperation};{x.Description}").ToArray();

            return messages;
        }
    }
}
